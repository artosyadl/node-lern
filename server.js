var bd = require('bd');
db.connect();

function run() {
  var vasya = new User("Vasya");
  var petya = new User("Petya");

  vasya.hello(petya);
  console.log(petya);
}

if (module.parent) {
  exports.run = run;
} else {
  run()
}


